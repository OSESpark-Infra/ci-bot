import * as base64 from 'std/encoding/base64.ts';

const { publicKey, privateKey } = await crypto.subtle.generateKey(
    {
        name: 'RSA-OAEP',
        modulusLength: 4096,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: 'SHA-256',
    },
    true,
    ['encrypt', 'decrypt'],
);
await Deno.writeTextFile(
    'key.json',
    JSON.stringify({
        publicKey: base64.encode(await crypto.subtle.exportKey('spki', publicKey)),
        privateKey: base64.encode(await crypto.subtle.exportKey('pkcs8', privateKey)),
    }),
);

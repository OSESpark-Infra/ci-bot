import { Status } from 'std/http/mod.ts';
import { createCommonResponse } from 'std/http/util.ts';

import { RequestEvent } from 'libs/http_server/mod.ts';

export class HealthRequestHandler implements EventListenerObject {
    handleEvent({ resolve }: RequestEvent) {
        resolve(createCommonResponse(Status.OK));
    }
}

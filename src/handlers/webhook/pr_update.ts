import { type PullRequestHook, WebhookAction } from 'libs/scm/mod.ts';

import { WebhookHandler } from './base.ts';

export class PullRequestUpdateHookHandler extends WebhookHandler {
    protected async _handle(webhook: PullRequestHook) {
        const { action, pr, change } = webhook;
        if (action === WebhookAction.PR.UPDATE && !['head', 'base'].includes(change ?? '')) {
            return;
        }
        if (!await this._util.checkCla(pr)) {
            return;
        }
        await this._util.runPipeline(pr);
    }
}

import type { Client, Commit, PullRequest } from 'libs/scm/mod.ts';

import { CLAClient } from '../../cla.ts';
import { Config } from '../../config.ts';
import { JenkinsClient } from '../../jenkins.ts';
import { getLogger } from '../../log.ts';

const CACHES = new Map<string, WebhookHandlerUtil>();

export class WebhookHandlerUtil {
    #log = getLogger();
    #scm;
    #cla;
    #claUrl;
    #jenkins;

    constructor(community: Config.Community, scm: Client) {
        const { name, cla: { check, externalUrl, id } } = community;
        const instance = CACHES.get(name);
        if (instance) {
            return instance;
        }
        this.#scm = scm;
        this.#cla = check ? new CLAClient(name) : null;
        this.#claUrl = `${externalUrl}/sign/${id}`;
        this.#jenkins = new JenkinsClient(name);
        CACHES.set(name, this);
    }

    async checkCla(pr: PullRequest, force = false) {
        const { target: repo, number } = pr;
        if (!this.#cla) {
            return true;
        }
        const commits = await this.#scm!.pullRequest({ repo, number }).getCommits() ?? [];
        const unsignedCommits = await this.#cla.check(commits, force);
        if (!unsignedCommits.length) {
            await this.#scm!.pullRequest().updateLabels({ add: ['cla/signed'], remove: ['cla/unsigned'] });
            return true;
        }
        this.#log.warning('CLA not signed');
        await Promise.all([
            this.#scm!.pullRequest().createComment({ body: this.#claMessage(unsignedCommits) }),
            this.#scm!.pullRequest().updateLabels({ add: ['cla/unsigned'], remove: ['cla/signed'] }),
        ]);
        return false;
    }

    async runPipeline(pr: PullRequest) {
        const { target: repo, state, head: { sha } } = pr;
        if (state !== 'merged') {
            const isCompleted = (await this.#scm!.checkRun({ repo, sha }).findBySha()).every(({ status }) =>
                status === 'completed'
            );
            if (!isCompleted && await this.#jenkins!.isBuilding(pr)) {
                this.#log.warning('job is already running');
                // TODO: prompt user to wait for previous job to finish
                return;
            }
        }
        const queueId = await this.#jenkins!.build(pr);
        if (queueId === null) {
            this.#log.error('failed to execute job');
            return;
        }
        this.#log.debug(`queueId: ${queueId}`);
        // TODO: poll and wait for job start
    }

    // TODO: customizable prompt
    #claMessage(unsignedCommits: Commit[]) {
        return `
感谢您的贡献！

以下提交的作者尚未签署贡献者许可协议 (CLA)：

${unsignedCommits.map(({ sha, message }) => `**${sha.slice(0, 8)}** | ${message.replaceAll('\n', ' ')}`).join('\n')}

您可以点击[**此处**](${this.#claUrl})签署 CLA。 签署 CLA 后，您需要评论\`/check-cla\`以再次检查 CLA 状态。
`.trim();
    }
}

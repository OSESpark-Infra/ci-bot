import type { Label, PullRequestHook } from 'libs/scm/mod.ts';

import { WebhookHandler } from './base.ts';

export class PullRequestApproveHookHandler extends WebhookHandler {
    protected async _handle(webhook: PullRequestHook) {
        const { pr: { target: repo, number, labels } } = webhook;
        if (this.#checkCla(labels ?? (await this._scm.pullRequest({ repo, number }).get()).labels!)) {
            if ((await this._scm.pullRequest().get()).isMergeable) {
                return this._scm.pullRequest().merge();
            }
        }
    }

    #checkCla(labels: Label[]) {
        return labels.some(({ name }) => name === 'cla/signed');
    }
}

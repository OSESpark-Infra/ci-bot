import { HttpMethod } from 'std/http/mod.ts';

import { getLogger } from './log.ts';

export type SimpleObject = Record<string, any>;

type JsonPrimitive = string | number | boolean | null;
export type JsonValue = JsonPrimitive | JsonValue[] | { [P in string]: JsonValue };
export type JsonArray = JsonValue[];
export type JsonObject = Record<string, JsonValue>;

export type ArrayElementType<T> = T extends (infer E)[] ? E : never;

export type MaybeAsync<T> = T | Promise<T>;

declare namespace TupleImpl {
    type IndexOf<T extends any[]> = Exclude<keyof T, keyof any[]>;
    type LShift<T extends any[]> = ((..._: T) => 0) extends ((..._: [T[0], ...infer R]) => 0) ? R : never;
    type Search<N extends number, U extends never[], V extends never[][]> = U['length'] extends N ? U
        : Search<N, [...U, ...V[0]][N] extends never ? U : [...U, ...V[0]], LShift<V>>;
    type Exp2<N extends number, U extends never[] = [never], V extends never[][] = [[]]> = U[N] extends never
        ? Search<N, V[0], LShift<V>>
        : Exp2<N, [...U, ...U], [U, ...V]>;
}
export type Tuple<T, N extends number> = Record<TupleImpl.IndexOf<TupleImpl.Exp2<N>>, T> & ReadonlyArray<T>;

export type Subtract<N extends number, M extends number> = TupleImpl.Exp2<N> extends [...TupleImpl.Exp2<M>, ...infer R]
    ? R['length']
    : number;

type U2I<U> = (U extends never ? never : (_: U) => never) extends (_: infer I) => void ? I : never;
type U2T<U, T extends any[] = []> = U2I<U extends never ? never : (_: U) => U> extends (_: never) => infer V
    ? U2T<Exclude<U, V>, [...T, V]>
    : T;

export type KeyOf<T> = T extends T ? keyof T : never;
export type ValueOf<T, K extends keyof T = KeyOf<T>> = T extends T ? T[K] : never;

declare global {
    interface ArrayConstructor {
        isArray<T>(arg: any): arg is T[];
    }

    interface ObjectConstructor {
        keys<T extends object>(o: T): U2T<KeyOf<T>>;
        assign<T extends {}, U>(target: T, ...sources: U[]): T & U;
        entries<T extends SimpleObject>(o: T): [Exclude<KeyOf<T>, symbol | number>, ValueOf<T>][];
        fromEntries<K extends string, T>(entries: Iterable<readonly [K, T]>): Record<K, T>;
        fromEntries<T extends SimpleObject>(entries: Iterable<readonly [keyof T, ValueOf<T>]>): T;
    }
}

type APIRequestInit = {
    method?: HttpMethod;
    params?: Record<string, any>;
    headers?: Record<string, string>;
    body?: BodyInit;
};

// TODO: REST API Client
export function createApi(pattern: string, defaultInit?: Partial<Omit<APIRequestInit, 'params' | 'body'>>) {
    const log = getLogger();
    return <N extends number>(segments: Tuple<string, N>, ...keys: Tuple<string, Subtract<N, 1>>) =>
    async (init: APIRequestInit) => {
        const { method, params, headers, body } = init;
        const url = `${pattern}${[segments[0], ...keys.flatMap((key, i) => [params![key], segments[i + 1]])].join('')}`;
        const req = new Request(url, {
            method: method ?? defaultInit?.method ?? 'GET',
            headers: Object.assign(defaultInit?.headers ?? {}, headers),
            body,
        });
        log.info(`API request:\n${asString(req)}`);
        if (body && ('string' === typeof body || body instanceof FormData || body instanceof URLSearchParams)) {
            log.debug(asString(body));
        }
        const resp = await fetch(req);
        log.info(`API response:\n${asString(resp)}`);
        return resp;
    };
}

function asString(data: any) {
    if (data instanceof Request) {
        return requestAsString(data);
    }
    if (data instanceof Response) {
        return responseAsString(data);
    }
    if (data instanceof Headers) {
        return headersAsString(data);
    }
    return `${data}`;
}

function requestAsString(req: Request) {
    const { method, url, headers } = req;
    return [`${method} ${url.replace(new URL(url).origin, '')}`, headersAsString(headers)].join('\n');
}

function responseAsString(resp: Response) {
    const { url, status, statusText, headers } = resp;
    return [url.replace(new URL(url).origin, ''), `${status} ${statusText}`, headersAsString(headers)].join('\n');
}

function headersAsString(headers: Headers) {
    return [...headers].map(([k, v]) => `${k}: ${v}`).join('\n');
}

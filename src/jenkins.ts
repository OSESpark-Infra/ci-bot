import * as base64 from 'std/encoding/base64.ts';
import { isSuccessfulStatus } from 'std/http/mod.ts';

import { PullRequest } from 'libs/scm/mod.ts';

import { Config } from './config.ts';
import { getLogger } from './log.ts';
import { createApi } from './utils.ts';

const CACHES = new Map<string, JenkinsClient>();
const PT_QUEUE_ITEM = new URLPattern({ pathname: '/jenkins/queue/item/:queueId/' });

export class JenkinsClient {
    #log = getLogger();
    #community;
    #api;

    constructor(community: string) {
        const instance = CACHES.get(community);
        if (instance) {
            return instance;
        }
        const config = new Config();
        this.#community = config.communities.get(community)!;
        // ({ api: this.#api, user: this.#user, token: this.#token } = config.jenkins);
        const { api: url, user, token } = config.jenkins;
        const api = createApi(`${url}`, { headers: { 'Authorization': `Basic ${base64.encode(`${user}:${token}`)}` } });
        this.#api = {
            build: api`/${'job'}/buildWithParameters`,
            state: api`/${'job'}/api/xml?tree=${'tree'}&xpath=${'xpath'}&wrapper=root`,
        };
        CACHES.set(community, this);
    }

    // TODO: class JekinsJobBuild
    async build(pr: PullRequest) {
        const { target: repoName, number, head, base, state } = pr;
        const { name, majun } = this.#community!;
        const { job, checks, tasks } = this.#community!.repos.get(repoName)!;
        return state === 'merged'
            ? await this.#build(`${job}.post-merge`, {
                COMMUNITY: name,
                MAJUN_COMMUNITY: majun,
                REPO: repoName,
                BRANCH: base.name,
                TASKS: JSON.stringify(tasks),
            })
            : await this.#build(job, {
                COMMUNITY: name,
                MAJUN_COMMUNITY: majun,
                TARGET_REPO: repoName,
                TARGET_BRANCH: base.name,
                TARGET_COMMIT: base.sha,
                SOURCE_BRANCH: head.name ?? '',
                SOURCE_COMMIT: head.sha ?? '',
                PULL_NUMBER: `${number}`,
                CHECKS: JSON.stringify(checks),
            });
    }

    async isBuilding(pr: PullRequest) {
        const { target: repoName, head: { sha } } = pr;
        const { job } = this.#community!.repos.get(repoName)!;
        return await (await this.#api!.state({
            params: {
                job: this.#jobParam(job),
                tree: encodeURIComponent('builds[actions[parameters[name,value]],building]'),
                xpath: encodeURIComponent(
                    `(/workflowJob/build[building="true" and action/parameter[name="SOURCE_COMMIT" and value="${sha}"]])[1]`,
                ),
            },
        })).text() !== '<root/>';
    }
    #jobParam(jobName: string) {
        return jobName.split('/').map((x) => `job/${x}`).join('/');
    }

    async #build(job: string, params?: Record<string, string>) {
        const resp = await this.#api!.build({
            method: 'POST',
            params: { job: this.#jobParam(job) },
            body: params ? new URLSearchParams(params) : undefined,
        });
        const { status, headers } = resp;
        if (!isSuccessfulStatus(status)) {
            this.#log.async.error(() => resp.text());
            return null;
        }
        return PT_QUEUE_ITEM.exec(headers.get('Location')!)?.pathname.groups?.['queueId'] ?? null;
    }
}

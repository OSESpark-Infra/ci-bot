import { Client, DriverKind } from 'libs/scm/mod.ts';

import { Config } from './config.ts';
import { getLogger } from './log.ts';

const CACHE: Map<string, Client> = new Map();

export const ScmClientFactory = {
    get(communityName: string) {
        let client = CACHE.get(communityName);
        if (client) {
            return client;
        }
        const log = getLogger();
        const community = new Config().communities.get(communityName);
        if (!community) {
            log.warning(`community not found: ${communityName}`);
            return null;
        }
        const { scm: { name: driver }, apiToken: api, webhookToken: webhook } = community;
        if (!Object.values(DriverKind).includes(driver as any)) {
            log.warning(`unsupported driver: ${driver}`);
            return null;
        }
        client = new Client(driver as DriverKind, { api, webhook });
        CACHE.set(communityName, client);
        return client;
    },
};

# syntax=docker/dockerfile:latest

ARG DENO_VERSION=1.36.0
ARG RUST_VERSION=1.70.0
ARG BIN_IMAGE=denoland/deno:bin-${DENO_VERSION}

FROM ${BIN_IMAGE} AS bin

FROM rust:${RUST_VERSION} AS build

WORKDIR /opt/src/ci-bot

RUN <<'EOF'
#!/bin/bash -xeu
apt-get update
apt-get install -y --no-install-recommends cmake protobuf-compiler
EOF
RUN --mount=type=cache,id=ci-bot,target=/opt/cache \
    <<'EOF'
#!/bin/bash -xeu
mkdir -p /opt/cache/{deno,dist}
EOF

COPY --link --from=bin /deno /usr/local/bin/deno
COPY --link . .

RUN --mount=type=cache,id=ci-bot,source=/deno,target=/root/.cache/deno \
    --mount=type=cache,id=ci-bot,source=/dist,target=./dist \
    <<'EOF'
#!/bin/bash -xeu
deno task cache
deno task bundle
EOF
RUN --mount=type=cache,id=ci-bot,source=/dist,target=./dist \
    <<'EOF'
cargo build --release --color=always -v
mv ./dist/release/ci-bot ./dist/ci-bot
objcopy --only-keep-debug ./dist/ci-bot ./dist/ci-bot.dbg
strip ./dist/ci-bot
objcopy --add-gnu-debuglink=./dist/ci-bot.dbg ./dist/ci-bot
cp ./dist/ci-bot /opt/ci-bot
EOF

FROM quay.io/curl/curl AS tini

ARG TINI_VERSION=0.19.0

RUN <<'EOF'
#!/bin/sh -xeu
curl -fsSL https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini -O
chmod +x tini
EOF

FROM gcr.io/distroless/cc-debian12:nonroot AS final

COPY --link --from=build /opt/ci-bot /usr/local/bin/ci-bot
COPY --link --from=tini /home/curl_user/tini /usr/local/bin/tini

WORKDIR /usr/local/etc/ci-bot
ENTRYPOINT [ "/usr/local/bin/tini", "--", "/usr/local/bin/ci-bot" ]
CMD [ "-f", "yaml", "config.yaml" ]
